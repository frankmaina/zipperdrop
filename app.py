from shutil import make_archive
import os
import dropbox
import webbrowser,time

# Get your app key and secret from the Dropbox developer website
app_key = 'enter key here'
app_secret = 'enter key here'

#begin auth
flow = dropbox.client.DropboxOAuth2FlowNoRedirect(app_key, app_secret)
print 'Click "Allow" (Once the authorisation page opens in the browser) and Copy the authorization code.'
authorize_url = flow.start()
webbrowser.open(authorize_url) #open in browser
code = raw_input("Enter the authorization code here: ").strip()
access_token, user_id = flow.finish(code)
client = dropbox.client.DropboxClient(access_token)
# finish auth

#begin compression
archive_name ='test'
root_dir = os.path.dirname('test/')
make_archive(archive_name, 'zip', root_dir)

#read file to upload online
f = open('test.zip','rb')
response = client.put_file('/Py-Builds/test.zip', f)
print "The file was successfully uploaded"
